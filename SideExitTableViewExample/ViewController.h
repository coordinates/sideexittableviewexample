//
//  ViewController.h
//  SideExitTableViewExample
//
//  Created by Masayoshi Ukida on 11/10/26.
//  Copyright (c) 2011年 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController

-(IBAction)touchButton:(id)sender;
@end
