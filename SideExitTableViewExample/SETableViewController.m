//
//  SETableViewController.m
//  SideExitTableViewExample
//
//  Created by Masayoshi Ukida on 11/10/26.
//  Copyright (c) 2011年 __MyCompanyName__. All rights reserved.
//

#import "SETableViewController.h"
#import "ViewController.h"

@interface SETableViewController()
-(void)showTableView:(BOOL)animated; 
-(void)hideTableView:(BOOL)animated;
@end

@implementation SETableViewController

@synthesize tableView = _tableView;
@synthesize previousButton = _previousButton;

static id _instance = nil;
static BOOL _willDelete = NO;

+ (id)sharedInstance
{
  @synchronized(self) {
    if (!_instance) {
      [[self alloc] init];
    }
  }
  return _instance;
}

+ (id)allocWithZone:(NSZone*)zone
{
  @synchronized(self) {
    if (!_instance) {
      _instance = [super allocWithZone:zone];
      return _instance;
    }
  }
  return nil;
}

+ (void)deleteInstance
{
  if (_instance) {
    @synchronized(_instance) {
      _willDelete = YES;
      [_instance release];
      _instance = nil;
      _willDelete = NO;
    }
  }
}

- (id)copyWithZone:(NSZone*)zone
{
  return self;
}

- (id)retain
{
  return self;
}

- (unsigned)retainCount
{
  return UINT_MAX;
}

- (id)autorelease
{
  return self;
}

- (oneway void)release
{
  @synchronized(self) {
    if (_willDelete) {
      [super release];
    }
  }
}

- (void)dealloc
{
  // このクラスの削除処理を記述
  
  [super dealloc];
}

#pragma mark - getter override

-(UITableView *)tableView {
  @synchronized(_tableView) {
    if (!_tableView) {
      CGRect tableViewFrame  = CGRectMake(0, 0, _previousViewController.view.frame.size.width*0.8, _previousViewController.view.frame.size.height);
      _tableView = [[UITableView alloc] initWithFrame:tableViewFrame];
      _tableView.delegate = self;
      _tableView.dataSource = self;
      [_tableView setShowsVerticalScrollIndicator:NO];
    }
  }
  return _tableView;
}

-(UIButton *)previousButton {
  @synchronized(_previousButton) {
    if (!_previousButton) {
      CGRect previousViewFrame = CGRectMake(_previousViewController.view.frame.size.width*0.8, 0, _previousViewController.view.frame.size.width*0.2, _previousViewController.view.frame.size.height);
      _previousButton = [[UIButton alloc] initWithFrame:previousViewFrame];
      [_previousButton addTarget:self action:@selector(touchPreviousButton:) forControlEvents:UIControlEventTouchUpInside];

    }
  }
  return _previousButton;
}


#pragma mark - View lifecycle

-(IBAction)touchPreviousButton:(id)sender {
  [self hideTableView:YES];
}


-(void)presentViewController:(UIViewController *) viewControllerToPresent animated:(BOOL)flag {
  _previousViewController = viewControllerToPresent;
  
  //[self.tableView scrollToNearestSelectedRowAtScrollPosition:UITableViewScrollPositionTop animated:NO];  

  [self showTableView:YES];
}


#pragma mark - private
-(void)showTableView:(BOOL)animated {
  [_previousViewController.view insertSubview:self.tableView atIndex:0];
  
  
  // start animation
  if(animated) {
    [UIView beginAnimations:nil context:nil];
  }
  
  for(int i=0; i<[_previousViewController.view.subviews count]; i++) {
    UIView *subView = (UIView *)[_previousViewController.view.subviews objectAtIndex:i];
    
    if([_tableView isEqual:subView]) {
      continue;
    }
    
    subView.transform = CGAffineTransformMakeTranslation(_previousViewController.view.frame.size.width*0.8, 0);
  }
  
  if(animated) {
    [UIView commitAnimations];  
  }
  
  [_previousViewController.view addSubview:self.previousButton];  
}

-(void)hideTableView:(BOOL)animated {
  
  if(animated) {
    [UIView beginAnimations:nil context:nil];
  }
  [UIView setAnimationDelegate:self]; 
  [UIView setAnimationDidStopSelector:@selector(animationDidStop:finished:context:)]; 
  
  for(int i=0; i<[_previousViewController.view.subviews count]; i++) {
    UIView *subView = (UIView *)[_previousViewController.view.subviews objectAtIndex:i];
    
    if([_previousButton isEqual:subView]) {
      continue;
    }
    if([_tableView isEqual:subView]) {
      continue;
    }
    
    subView.transform = CGAffineTransformMakeTranslation(0, 0);
  }
  
  if(animated) {
    [UIView commitAnimations];  
  }  
  
}

-(void)animationDidStop:(NSString *)animationID finished:(NSNumber *)finished context:(void *)context {
  
  [_tableView removeFromSuperview];
  
  [_previousButton removeFromSuperview];
}



#pragma mark - UITableViewDelegate & UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
  return 20;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
  
  static NSString *CellIdentifier = @"Cell";
  
  UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
  if (cell == nil) {
    cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
  }
  
  // Configure the cell...
  [cell.textLabel setText:[NSString stringWithFormat:@"test:%d", indexPath.row]];
                
  
  return  cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
  
  ViewController *viewController = [[ViewController alloc] initWithNibName:@"ViewController" bundle:nil];
  
  [UIApplication sharedApplication].delegate.window.rootViewController = viewController;
  _previousViewController = viewController;
  [self showTableView:NO];
  [self hideTableView:YES];
}


@end
