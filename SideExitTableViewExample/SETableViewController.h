//
//  SETableViewController.h
//  SideExitTableViewExample
//
//  Created by Masayoshi Ukida on 11/10/26.
//  Copyright (c) 2011年 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SETableViewController : NSObject <UITableViewDelegate, UITableViewDataSource>
{
  UITableView *_tableView;
  UIButton *_previousButton;
  UIViewController *_previousViewController;
}

@property (nonatomic, retain) UITableView *tableView;
@property (nonatomic, retain) UIButton *previousButton;

+ (id)sharedInstance;
-(void)presentViewController:(UIViewController *) viewControllerToPresent animated:(BOOL)flag;
@end
